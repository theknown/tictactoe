#include "includes.h"

CPU::CPU(){}

CPU::CPU(int x_min, int y_min, int x_max, int y_max, int radius): x_min(x_min), y_min(y_min), x_max(x_max), y_max(y_max),radius(radius){}

CPU::CPU(CPU& cpu){
    this->x_min = cpu.x_min;
    this->y_min = cpu.y_min;
    this->x_max = cpu.x_max;
    this->y_max = cpu.y_max;
    this->radius = cpu.radius;
}

void CPU::setCPU(CPU cpu){
    this->x_min = cpu.x_min;
    this->y_min = cpu.y_min;
    this->x_max = cpu.x_max;
    this->y_max = cpu.y_max;
    this->radius = cpu.radius;
}

void CPU::draw(){
    al_draw_circle((x_min+x_max)/2,(y_min+y_max)/2,radius,al_map_rgb(255,255,255),2);
}