#pragma once
#ifndef __UI__
#define __UI__
#include "includes.h"

class UI: public Object{
    int x_min = 620;
    int y_min = 330;
    int x_max = 740;
    int y_max = 450;
    Frame* frame;
    User* user;
    CPU* cpu;

public:
    UI();
    virtual void draw();    
};

#endif