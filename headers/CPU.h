#pragma once
#ifndef __CPU__
#define __CPU__
#include "includes.h"

class CPU{
    int x_min,y_min;
    int x_max,y_max;
    int radius;

public:
    CPU();
    CPU(int x_min, int y_min, int x_max, int y_max, int radius);
    CPU(CPU& cpu);
    void setCPU(CPU cpu);
    virtual void draw();
};

#endif