#pragma once
#ifndef __FRAME__
#define __FRAME__
#include "includes.h"

class Frame: public Object{
    int x_initial,y_initial,x_final,y_final,distance;

public:
    Frame();
    Frame(int x_initial,int y_initial,int x_final,int y_final,int distance);
    Frame(Frame& frame);
    void setFrame(Frame frame);
    virtual void draw();

};


#endif