#pragma once

#ifndef __OBJECT__
#define __OBJECT__

class Object{
public:
    virtual void draw() = 0;
    virtual ~Object(){}
};

#endif