#include "includes.h"

Frame::Frame(){};

Frame::Frame(int x_initial,int y_initial,int x_final,int y_final,int distance):x_initial(x_initial),y_initial(y_initial),x_final(x_final),y_final(y_final),distance(distance){};

Frame::Frame(Frame& frame){
    this->x_initial = frame.x_initial;
    this->y_initial = frame.y_initial;
    this->x_final = frame.x_final;
    this->y_final = frame.y_final; 
    this->distance = frame.distance;
}

void Frame::setFrame(Frame frame){
    this->x_initial = frame.x_initial;
    this->y_initial = frame.y_initial;
    this->x_final = frame.x_final;
    this->y_final = frame.y_final;
    this->distance = frame.distance;
}

void Frame::draw(){
    al_draw_line(x_initial,y_initial+distance,x_final,y_initial+distance,al_map_rgb(255,255,255),2);
    al_draw_line(x_initial,y_initial+(2*distance),x_final,y_initial+(2*distance),al_map_rgb(255,255,255),2);
    al_draw_line(x_initial+distance,y_initial,x_initial+distance,y_final,al_map_rgb(255,255,255),2);
    al_draw_line(x_initial+(2*distance),y_initial,x_initial+(2*distance),y_final,al_map_rgb(255,255,255),2);
}