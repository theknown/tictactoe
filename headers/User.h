#pragma once
#ifndef __USER__
#define __USER__
#include "includes.h"

class User{
    int x_min, y_min;
    int x_max, y_max;

public:
    User();
    User(int x_min, int y_min, int x_max, int y_max);
    User(User& user);
    void setUser(User user);
    virtual void draw();
};

#endif