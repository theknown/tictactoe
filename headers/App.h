#pragma once
#ifndef __App__
#define __App__
#include "includes.h"

class App{
    int w,h;
    ALLEGRO_MONITOR_INFO *info;
    ALLEGRO_DISPLAY *display;
    // ALLEGRO_EVENT_QUEUE* event_queue;
    // ALLEGRO_TIMER *timer;
    Frame* frame;
    // ALLEGRO_FONT* font;
    bool done;
    int x_min = 360;    // Change these values for size of #, make sure it's a square grid 
    int y_min = 230;
    int x_max = 680;
    int y_max = 550;
    int distance = (x_max - x_min)/3;  // make sure it divides the square into three equal halves

public:
    App();
    bool run();
};
#endif