#include "includes.h"

User::User(){}

User::User(int x_min, int y_min, int x_max, int y_max): x_min(x_min), y_min(y_min), x_max(x_max), y_max(y_max){}

User::User(User& user){
    this->x_min = user.x_min;
    this->y_min = user.y_min;
    this->x_max = user.x_max;
    this->y_max = user.y_max;
}

void User::setUser(User user){
    this->x_min = user.x_min;
    this->y_min = user.y_min;
    this->x_max = user.x_max;
    this->y_max = user.y_max;
}

void User::draw(){
    al_draw_line(x_min,y_min,x_max,y_max,al_map_rgb(0,0,0),2);
    al_draw_line(x_min,y_max,x_max,y_min,al_map_rgb(0,0,0),2);
}